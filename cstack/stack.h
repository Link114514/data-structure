#pragma once
#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>
#include <assert.h>
typedef char* SElemType;//栈数据类型
typedef struct Stack
{
	SElemType* a;
	int TOP;		// 栈顶
	int capacity;  // 容量 
}ST;
// 初始化栈 
void StackInit(ST* ps);
// 入栈 
void StackPush(ST* ps, SElemType data);
// 出栈 
void StackPop(ST* ps);
// 获取栈顶元素 
SElemType StackTop(ST* ps);
// 获取栈中有效元素个数 
int StackSize(ST* ps);
// 销毁栈 
void StackDestroy(ST* ps);

void StackPrint(ST* ps);

//判断是否为空
bool Stackbool(ST* ps);
