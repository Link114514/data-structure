#pragma once
#define  _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <errno.h> 
#include <string.h>
#include <assert.h>
typedef int LtDataType;
typedef struct Ltnode Ltnode;
struct Ltnode
{
	LtDataType data;
	struct Ltnode* prev;
	struct Ltnode* next;
};
Ltnode* LTInit();
Ltnode* BuyNode(LtDataType x);
void LTPrint(Ltnode* phead);
void LTPushBack(Ltnode* phead, LtDataType x);
 
void PopFront(Ltnode* phead);
void PopBack(Ltnode* phead);
void LTInsertAfter(Ltnode* pos, LtDataType x);
void LTDestroy(Ltnode* phead);
void LTDestroy2(Ltnode** pphead);
void ListErase(Ltnode* pos);
Ltnode* LTFind(Ltnode* phead, LtDataType x);

void LTErase(Ltnode* pos);