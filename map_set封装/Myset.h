#pragma once
#include "RBtree_bit.h"
 namespace kp
{
	 using namespace std;
	 template<class K>
	 class set
	 {
		 struct KeyOfSet
		 {
			 const K& operator()(const K &data)
			 {
				 return data;
			 }
		 };
	 public:
		 typedef typename RBTree<K, const K, KeyOfSet> ::iterator iterator;
		 bool insert(const K& data)
		 {
			 return _set.Insert();
		 }
		 iterator begin()
		 {
			 return _set.begin();
		 }
		 iterator end()
		 {
			 return _set.end();
		 }

	 private:
		 RBTree<K,const K,KeyOfSet> _set;
	 };

}