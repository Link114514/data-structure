#pragma once
#include <stdio.h>
#include <String.h>
#include <math.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
//void HeapInit(Heap* hp);
//void HeapPush(Heap* hp, HeapDataType x);
//void HeapPop(Heap* hp);
//void HeapDestory(Heap* hp);

//void AdjustUp(HeapDataType* a, int child);
//HeapDataType HeapTop(Heap* hp);
//size_t HpSize(Heap* hp);
//bool HeapEmpty(Heap* hp);
//void Swap(int* a, int* b);
//void ShellSort(int* a, int n);
//void DigQsort(int* arr, int begin, int end);
void AdjustDown(int* a, int size, int parent);
void Swap(int* p1, int* p2);
void InsertSort(int* a, int n);
void PrintSort(int* a, int n);
void ShellSort1(int* a, int n);
void QuickSort(int* a, int begin, int end);
int* numcreate(int n);
void SelectSort(int* a, int n);
void MergeSort(int* a, int n);
void HeapSort(int* a, int n);



