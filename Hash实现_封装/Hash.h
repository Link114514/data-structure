//#pragma once
//#include <iostream>
//#include <vector>
//using namespace std;
//template<class K>
//struct HashFunc
//{
//	size_t operator()(const K& key)
//	{
//		return (size_t)key;
//	}
//};
//
//// 特化
//template<>
//struct HashFunc<string>
//{
//	size_t operator()(const string& s)
//	{
//		size_t hash = 0;
//		for (auto e : s)
//		{
//			hash += e;
//			hash *= 131;
//		}
//		return hash;
//	}
//};
//
//namespace Hash_Bucket {
//	template<class T>
//	struct HashData
//	{
//		HashData<T>* _Next;
//		T _Data;
//		HashData(const T& Data)
//			:_Next(nullptr)
//			,_Data(Data)
//		{}
//	};
//
//	template<class K, class T,class Hashcom,class KeyOfT>
//	class HashTable
//	{
//		typedef HashData<T> Node;
//		typedef class HashTable<K, T, Hashcom,KeyOfT> Self;
//	public:
//		HashTable()
//		{
//			_hashtable.resize(10, nullptr);
//			_size=0;
//		}
//		~HashTable()
//		{
//			clear();
//		}
//		Node* Find(const K& data)
//		{
//			Hashcom com;
//			KeyOfT Kot;
//			size_t hashi = com(Kot(data)) % _hashtable.size();
//			Node* cur = _hashtable[hashi];
//			while(cur)
//			{
//				if (cur->_Data.first==data)
//				{
//					return cur;
//				}
//				cur = cur->_Next;
//			}
//			return nullptr;
//		}
//
//		bool insert(const K& data)
//		{
//			if (Find(data))
//				return false;
//			Hashcom com;
//			if (_size == _hashtable.size())//达到负载因子需要扩容
//			{
//				vector<Node*>NewHashTable(_hashtable.size() * 2,nullptr);
//				for (size_t i = 0; i < _hashtable.size(); i++)
//				{
//					Node* cur = _hashtable[i];//找桶
//					while (cur)
//					{
//						Node* next = cur->_Next;
//
//						size_t hashi = com(cur->_Data.first) % NewHashTable.size();//重新映射
//						cur->_Next = NewHashTable[hashi];
//						NewHashTable[hashi] = cur;
//
//						cur = next;//头插
//					}
//					_hashtable[i] = nullptr;
//				}
//				_hashtable.swap(NewHashTable);
//			}
//				size_t hashi = com(data.first) % _hashtable.size();
//				Node* newnode = new Node(data);
//				//头插
//				newnode->_Next = _hashtable[hashi];
//				_hashtable[hashi] = newnode;
//				_size++;
//				return true;	
//		}
//		bool Erase(const K& data)
//		{
//			Hashcom com;
//			size_t hashi = com(data) % _hashtable.size();
//			Node* cur = _hashtable[hashi];
//			Node* prev = nullptr;
//			while (cur)
//			{
//				if (cur->_Data.first == data)
//				{
//					if (prev)
//					{
//						prev->_Next = cur->_Next;
//					}
//					else
//					{
//						_hashtable[hashi] = cur->_Next;//为头
//					}
//					delete cur;
//					--_size;
//					return true;
//				}
//				prev = cur;
//				cur = cur->_Next;
//			}
//			return false;
//		}
//		size_t size()const
//		{
//			return _size;
//		}
//		bool Empty()const
//		{
//			return _size == 0;
//		}
//		size_t BucketCount()const
//		{
//			return _hashtable.capacity();
//		}
//		void clear()
//		{
//			for (size_t i= 0; i < _hashtable.size(); i++)
//			{
//				Node* cur = _hashtable[i];
//				while (cur)
//				{
//					Node* next = cur->_Next;
//					delete cur;
//
//					cur = next;
//				}
//				_hashtable[i] = nullptr;
//			}
//		}
//	private:
//		vector<Node*> _hashtable;
//		size_t _size;
//	};
//}
//
//
//namespace Close_Hash
//{
//	enum State{ EMPTY, EXIST, DELETE };
//
//	template<class K, class V>
//	struct Elem
//	{
//		pair<K, V> _val;
//		State _state;
//	};
//	template<class K, class V, class Hashfuc = HashFunc<string>>
//	class HashTable
//	{
//		
//	public:
//		typedef Elem<K,V> Node;
//
//		HashTable(size_t capacity = 3)
//			: _ht(capacity), _size(0), _totalSize(0)
//		{
//			for (size_t i = 0; i < capacity; ++i)
//				_ht[i]._state = EMPTY;
//		}
//
//		// 插入
//		bool Insert(const pair<K, V>& val)
//		{
//			if (Find(val))
//				return false;
//			if (_totalSize * 10 / _ht.size == 7)
//			{
//				vector<Elem> newht(_ht.size() * 2);
//				for (auto& ch : _ht)
//				{
//					if (ch._state == EXIST)
//					{
//						newht.insert(ch._val);
//					}
//				}
//				_ht.swap(newht);
//			}
//			Hashfuc hs;
//			size_t hashi = hs(val.first) % _ht.size();
//			while (_ht[hashi]._state == EXIST)
//			{
//				++hashi;
//				hashi %= _ht.size();
//			}
//			_ht[hashi]._val= val;
//			_ht[hashi]._state = EXIST;
//			++_totalSize;
//			return true;
//		}
//
//		// 查找
//		Node* Find(const K& key) {
//			Hashfuc hs;
//			size_t hashi = hs(key) % _ht.size();
//			while (_ht[hashi]._state == EMPTY)
//			{
//				if (key ==_ht[hashi].first &&_ht[hashi]._state==EXIST)
//				{
//					return &_ht[hashi];
//				}
//				++hashi;
//				hashi %= _ht.size();
//			}
//			return nullptr;
//		}
//
//		// 删除
//		bool Erase(const K& key)
//		{
//			Node *hashi = Find(key);
//			if (hashi)
//			{
//				hashi->_state ==DELETE;
//				--_ht.size();
//			}
//			else
//			{
//				return false;
//			}
//
//		}
//
//		size_t Size()const
//		{
//			return _size;
//		}
//
//		bool Empty() const
//		{
//			return _size == 0;
//		}
//
//		void Swap(HashTable<K, V>& ht)
//		{
//			swap(_size, ht._size);
//			swap(_totalSize, ht._totalSize);
//			_ht.swap(ht._ht);
//		}
//		
//	private:
//		size_t HashFunc(const K& key)
//		{
//			return key % _ht.capacity();
//		}
//		//void CheckCapacity();
//		vector<Node*> _ht;
//		size_t _size;
//		size_t _totalSize;  // 哈希表中的所有元素：有效和已删除, 扩容时候要用到
//	};
//}