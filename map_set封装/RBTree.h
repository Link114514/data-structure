#pragma once
#include <iostream>
#include <string>
using namespace std;
enum Colour
{
	RED,
	BLACK
};

template <class T>
struct RBTreeNode
{
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	T  _data;
	Colour _col;
	RBTreeNode(const T& data)
		:_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,_data(data)
		,_col(RED)//新插入的节点都是RED
		
	{}
};

template<class T>
class RBTreeIterator
{
	typedef RBTreeIterator<T> Self;
	typedef RBTreeNode<T> Node;
	RBTreeIterator(Node*node)
		:_node(node)
	{}
	
	T operator*()
	{
		return _node->_data
	}
	T operator->()
	{
		return &_node->_data;
	}

	Self* operator++()
	{
		//判断自己的右树是否存在--看自己是不是当前的根节点
		if (_node->_right)
		{
			Node* subR = _node->_right;
			while (subR && subR->_left)
			{
				subR = subR->_left;
			}
			return subR;
		}
		else
		{
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && cur == parent->_right)
			{
				parent = parent->_parent;
				cur = cur->_parent;
			}
			_node=parent;
		}
		return *this;
	}
	Self*operator--()
	{
		DeIncreament(_node);
		return *this;
	}

	bool operator!=(const Self&s)
	{
		return _node != s.node;
	}
	bool operator==(const Self&s)
	{
		return _node == s.node;
	}
private:
	Node* _node;
	void DeIncreament(Node*node)
	{
		// 处理迭代器在end的位置
		if (node->_parent->_parent == node && node->_col == RED)
			node = node->_right;
		else if (node->_left)
		{
			node = node->_left;
			while (node->_right)
				node = node->_right;
		}
		else
		{
			Node* pParent = node->_parent;
			while (pParent->_left == node)
			{
				node = pParent;
				pParent = node->_parent;
			}
			node = pParent;
		}
	}
};

template<class K,class T,class KeyOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;
public:
	typedef RBTreeIterator<T> Iterator;

	bool insert(const T& data)
	{
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return true;
		}
		else
		{
			Node* parent = nullptr;
			Node* cur = _root;
			KeyOfT kot;
			while (cur)
			{
				if (kot(cur->_data)<kot(data))
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (kot(cur->_data)>kot(data))
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}
			cur = new Node(data);
			if (kot(parent->_data)<kot(data))
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			cur->_parent = parent;
			//.
			while (parent && parent->_col == RED)//红黑树逻辑部分
			{
				//祖父节点
				Node* grandparent = parent->_parent;
				if (parent == grandparent->_left)//看叔叔节点颜色
				{
					Node* uncle = grandparent->_right;
					if (uncle && uncle->_col == RED)//存在且为红
					{
						uncle->_col = parent->_col = BLACK;
						grandparent->_col = RED;
						//向上继续处理
						cur = grandparent;
						parent = cur->_parent;
					}
					else
					{
						//叔叔不存在或者存在且为黑
						//旋转
						if (cur == parent->_left)
						{
							//     g         
							//  p     u    
							//c
							RotateR(grandparent);
							parent->_col = BLACK;
							grandparent->_col = RED;
						}
						else
						{
							//     g
							//  u     p
							//           c
							RotateL(parent);
							RotateR(grandparent);
							cur->_col = BLACK;
							grandparent->_col = RED;
						}
						break;
					}
				}
				else 
				{
					Node* uncle = grandparent->_left;
					if (uncle && uncle->_col == RED)
					{
						uncle->_col = parent->_col = BLACK;
						grandparent->_col = RED;
						cur = grandparent;
						parent = cur->_parent;
					}
					else
					{
						//叔叔不存在或者存在且为黑
						//旋转
						if (cur == parent->_right)
						{
							//     g
							//  u     p
							//           c
							RotateL(parent);
							parent->_col = BLACK;
							grandparent->_col = RED;
						}
						else
						{
							//      g
							//   p     u
							//c           
							RotateR(parent);
							RotateL(grandparent);
							cur->_col = BLACK;
							grandparent->_col = RED;
						}
						break;
					}
				}
			}
			_root->_col = BLACK;
			return true;
		}
	}
	/*Iterator find(const T& data) 
	{
		Node* cur = _root;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				cur=cur->_right
			}
			else if (kot(cur->_data) > kot(data))
			{
				cur = cur->_left;
			}
			else
			{
				return cur;
			}
		}
		return end();
	}
	Node* GetLeftMost()
	{
		Node* cur = _root;
		while (cur && cur->_left)
		{
			cur = cur->_left;
		}
		return cur;
	}
	Node* GetRightMost()
	{
		Node* cur = _root;
		while (cur && cur->_right)
		{
			cur = cur->_right;
		}
		return cur;
	}*/

	//bool Isbalence()
	//{
	//	if (_root && _root->_col == RED)
	//		return false;
	//	return Check(_root);
	//}
	void inorder()
	{
		_Inorder(_root);
	}
	Iterator* begin()
	{
		Node* subL = _root;
		while (subL&&subL->_left)
		{
			subL = subL->_left;
		}
		return Iterator(subL);
	}
	Iterator* end()
	{
		return Iterator(nullptr);
	}
	private:
		bool Check(Node* root)
		{
			if (root == nullptr)
				return false;
			Node* cur = root;
			if (cur->_col == RED && cur->_parent == RED)
			{
				cout << "存在连续的红色节点" << endl;
				return false;
			}
			return Check(root->_left) && Check(root->_right);
		}
		void _Inorder(Node* root)
		{
			if (root == nullptr)
				return;
			_Inorder(root->_left);
			cout << root->_kv.first << " " << root->_kv.second << endl;
			_Inorder(root->_right);
		}
		void RotateL(Node * parent)
		{
			Node* subR = parent->_right;
			Node* subRL = subR->_left;

			parent->_right = subRL;
			if (subRL)
				subRL->_parent = parent;

			subR->_left = parent;
			Node* ppnode = parent->_parent;
			parent->_parent = subR;

			if (parent == _root)
			{
				_root = subR;
				subR->_parent = nullptr;
			}
			else
			{
				if (ppnode->_left == parent)
				{
					ppnode->_left = subR;
				}
				else
				{
					ppnode->_right = subR;
				}
				subR->_parent = ppnode;
			}
			
		}
		void RotateR(Node * parent)
		{
			Node* subL = parent->_left;
			Node* subLR = subL->_right;

			parent->_left = subLR;
			if (subLR)
				subLR->_parent = parent;

			subL->_right = parent;

			Node* ppnode = parent->_parent;
			parent->_parent = subL;

			if (parent == _root)
			{
				_root = subL;
				subL->_parent = nullptr;
			}
			else
			{
				if (ppnode->_left == parent)
				{
					ppnode->_left = subL;
				}
				else
				{
					ppnode->_right = subL;
				}
				subL->_parent = ppnode;
			}
		}
		Node* _root = nullptr;
};