#include "stack.h"
#include <stdio.h>
//void mission1()
//{
//    char* china = "china";
//    char* Japan = "Japan";
//    char* France = "France";
//    char* India = "India";
//    char* Australia = "Australia";
//
//    ST stack;
//    StackInit(&stack);
//    StackPush(&stack, china);
//    StackPush(&stack, France);
//    StackPush(&stack, Australia);
//    StackPrint(&stack);
//    SElemType top = StackTop(&stack);
//    printf("Stack top: %s\n", top);
//    printf("-------------\n");
//    StackPop(&stack);
//    StackPop(&stack);
//    StackPrint(&stack);
//
//    SElemType top2 = StackTop(&stack);
//    printf("Stack top: %s\n", top);
//
//    StackDestroy(&stack);
//
//}
//
//bool isPalindrome(const char* str) {
//    int len = strlen(str);
//    ST stack;
//    StackInit(&stack);
//
//    // 将字符串前半部分压入栈中
//    for (int i = 0; i < len / 2; ++i) {
//        StackPush(&stack, str[i]);
//    }
//
//    // 如果字符串长度是奇数，跳过中间的字符
//    int start = (len % 2 == 0) ? len / 2 : len / 2 + 1;
//
//    // 检查字符串后半部分与栈中的字符是否匹配
//    for (int i = start; i < len; ++i) {
//        if (str[i] != StackTop(&stack)) {
//            StackDestroy(&stack);
//            return false;
//        }
//        StackPop(&stack);
//    }
//
//    StackDestroy(&stack);
//    return true;
//}
//void misson29()
//{
//    const char* testStr1 = "radar";
//    const char* testStr2 = "hello";
//    const char* testStr3 = "level";
//
//    printf("\"%s\" is palindrome: %s\n", testStr1, isPalindrome(testStr1) ? "true" : "false");
//    printf("\"%s\" is palindrome: %s\n", testStr2, isPalindrome(testStr2) ? "true" : "false");
//    printf("\"%s\" is palindrome: %s\n", testStr3, isPalindrome(testStr3) ? "true" : "false");
//}
void printOctal(int num) {
    ST stack;
    StackInit(&stack);

    if (num == 0) {
        printf("0");
    }
    else {
        while (num > 0) {
            StackPush(&stack, num % 8);
            num /= 8;
        }

        while (!Stackbool(&stack)) {
            printf("%d", StackTop(&stack));
            StackPop(&stack);
        }
    }

    StackDestroy(&stack);
    printf("\n");
}
int main()
{
    int num;

    printf("请输入一个非负十进制整数：");
    scanf("%d", &num);

    if (num < 0) {
        printf("请输入非负整数！\n");
    }
    else {
        printf("等值的八进制数为：");
        printOctal(num);
    }

    return 0;
}