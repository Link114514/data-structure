#pragma once
#include "Hash2 .h"
namespace kp {
	template<class K, class V>
	class unordered_map
	{
		struct KeyOfMap
		{
			K& operator()(const pair<K, V>& data)
			{
				return data.first;
			}
		};
		struct Hashcom
		{
			size_t operator()(const K& data)
			{
				return (size_t(data));
			}
		};
	public:
		typedef typename Hash_Bucket::HashTable<K, const pair<K, V>, KeyOfMap, Hashcom>::iterator iterator;

		iterator begin()
		{
			return _ht.begin();
		}
		iterator end()
		{
			return _ht.end();
		}
		bool insert(const pair<K, V>& data)
		{
			return _ht.insert(data);
		}
		iterator find(const pair<K, V>& data)
		{
			return _ht.Find(data);
		}
		bool erase(const pair<K, V>& data)
		{
			return _ht.Erase(data);
		}

	private:
		Hash_Bucket::HashTable<K, const pair<K, V>, KeyOfMap, Hashcom> _ht;
	};

}