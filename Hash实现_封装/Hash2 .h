#pragma once
#include <iostream>
#include <vector>
using namespace std;
template <class K>
struct HashFunc
{
	size_t operator()(const K& data) 
	{
		return (size_t(data));
	}
};

template<>//特化
struct HashFunc<string>
{
	size_t operator()(const string& data)
	{
		size_t hash = 0;
		for (auto& e : data)
		{
			hash += e;
			hash *= 131;//BKDR哈希算法
		}
		return hash;
	}
};

namespace Hash_Bucket {
	template<class T>
	struct HashData
	{
		HashData<T>* _Next;
		T _Data;
		HashData(const T& Data)
			:_Next(nullptr)
			,_Data(Data)
		{}
	};
	template<class K, class T,class KeyOfT,class Hashcom=HashFunc<string>>
	class HashTable;//前置声明
	template<class K,class T,class KeyOfT,class Hashcom=HashFunc<string>>
	struct Hash_iterator
	{
		typedef HashTable<K,T,KeyOfT,Hashcom> Ht;
		typedef Hash_iterator<K,T,KeyOfT> Self;
		typedef HashData<T> Node;
		Hash_iterator(Node*node,Ht*ht)
			:_node(node)
			,_ht(ht)
		{}
	public:
		T& operator*()
		{
			return _node->_Data;
		}
		T* operator->()
		{
			return &_node->_Data;
		}
		Self& operator++()
		{
			if (_node->_Next)
			{
				_node = _node->_Next;
			}
			else
			{
				Hashcom Hs;
				KeyOfT KOT;
				size_t hashi = (Hs(KOT(_node->_Data))) % _ht->_hashtable.size();
				hashi++;
				while (hashi < _ht->_hashtable.size())
				{
					if (_ht[hashi])
					{
						_node = _ht->_hashtable[hashi];
						break;
					}
					else
						hashi++;
				}
				if (hashi == _ht->_hashtable.size())
				{
					//到满了
					return nullptr;
				}
			}
			return *this;
		}
		bool operator==(const Self& s)
		{
			return _ht == s._ht;
		}
		bool operator=(const Self&& s)
		{
			swap(s);
			return *this;
		}
	private:
		Ht* _ht;
		Node* _node;
	};

	template<class K, class T, class KeyOfT, class Hashcom>
	class HashTable
	{
		typedef HashData<T> Node;
		typedef  HashTable<K, T,KeyOfT,Hashcom> Self;
		template<class K, class T, class KeyOfT, class Hashcom>
		friend  struct Hash_iterator;
	public:
		typedef Hash_iterator<K,T,KeyOfT,Hashcom> iterator;
		typedef Hash_iterator<const K, const T, KeyOfT, Hashcom> const_iterator;
		HashTable()
		{
			_hashtable.resize(10, nullptr);
			_size=0;
		}
		~HashTable()
		{
			clear();
		}
		const iterator begin()
		{
			for (int i = 0; i < _hashtable.size(); i++)
			{
				if (_hashtable[i])return iterator(_hashtable[i],this);
			}
			return end();
		}
		const iterator end()noexcept
		{
			return iterator(nullptr);
		}
		iterator* Find(const K& data)
		{
			Hashcom com;
			size_t hashi = com(data) % _hashtable.size();
			Node* cur = _hashtable[hashi];
			while(cur)
			{
				if (cur->_Data.first==data)
				{
					return iterator(cur);
				}
				cur = cur->_Next;
			}
			return nullptr;
		}

		bool insert(const K& data)
		{
			if (Find(data))
				return false;
			Hashcom com;
			if (_size == _hashtable.size())//达到负载因子需要扩容
			{
				vector<Node*>NewHashTable(_hashtable.size() * 2,nullptr);
				for (size_t i = 0; i < _hashtable.size(); i++)
				{
					Node* cur = _hashtable[i];//找桶
					while (cur)
					{
						Node* next = cur->_Next;

						size_t hashi = com(cur->_Data.first) % NewHashTable.size();//重新映射
						cur->_Next = NewHashTable[hashi];
						NewHashTable[hashi] = cur;

						cur = next;//头插
					}
					_hashtable[i] = nullptr;
				}
				_hashtable.swap(NewHashTable);
			}
				size_t hashi = com(data.first) % _hashtable.size();
				Node* newnode = new Node(data);
				//头插
				newnode->_Next = _hashtable[hashi];
				_hashtable[hashi] = newnode;
				_size++;
				return true;	
		}
		bool Erase(const K& data)
		{
			Hashcom com;
			size_t hashi = com(data) % _hashtable.size();
			Node* cur = _hashtable[hashi];
			Node* prev = nullptr;
			while (cur)
			{
				if (cur->_Data.first == data)
				{
					if (prev)
					{
						prev->_Next = cur->_Next;
					}
					else
					{
						_hashtable[hashi] = cur->_Next;//为头
					}
					delete cur;
					--_size;
					return true;
				}
				prev = cur;
				cur = cur->_Next;
			}
			return false;
		}
		size_t size()const
		{
			return _size;
		}
		bool Empty()const
		{
			return _size == 0;
		}
		size_t BucketCount()const
		{
			return _hashtable.capacity();
		}
		void clear()
		{
			for (size_t i= 0; i < _hashtable.size(); i++)
			{
				Node* cur = _hashtable[i];
				while (cur)
				{
					Node* next = cur->_Next;
					delete cur;

					cur = next;
				}
				_hashtable[i] = nullptr;
			}
		}
		vector<Node*> GetHashtable() //可友元
		{
			return _hashtable;
		}
	private:
		vector<Node*> _hashtable;
		size_t _size;
	};
}






namespace Close_Hash//闭值法
{
	enum State{ EMPTY, EXIST, DELETE };

	template<class K, class V>
	struct HashData
	{
		pair<K, V> _val;
		State _state=EMPTY;
	};
	template<class K, class V, class Hashfuc = HashFunc<string>>
	class HashTable
	{
		
	public:
		typedef HashData<K,V> Node;

		HashTable(size_t capacity = 3)
			: _ht(capacity), _size(0), _totalSize(0)
		{
			for (size_t i = 0; i < capacity; ++i)
				_ht[i]._state = EMPTY;
		}

		// 插入
		bool Insert(const pair<K, V>& val)
		{
			if (Find(val.first))
				return false;
			if (_totalSize * 10 / _ht.size >= 7)//扩容因子>0.7
			{
				HashTable<K,V,Hashfuc> newht(_ht.size() * 2);//扩容 重新映射
				for (auto& ch : _ht)
				{
					if (ch._state == EXIST)
					{
						newht.Insert(ch._val);//不会产生递归,因为扩容后不插入扩容因子不会达到0.7
					}
				}
				_ht.swap(newht);//和原表交换
			}

			Hashfuc hs;
			size_t hashi = hs(val.first) % _ht.size();
			while (_ht[hashi]._state == EXIST)
			{
				++hashi;
				hashi %= _ht.size();
			}
			_ht[hashi]._val= val;
			_ht[hashi]._state = EXIST;
			++_totalSize;
			return true;
		}

		// 查找
		Node* Find(const K& key) {
			Hashfuc hs;
			size_t hashi = hs(key) % _ht.size();//哈希映射
			while (_ht[hashi]._state == EMPTY)//空
			{
				if (key ==_ht[hashi].first 
					&&_ht[hashi]._state==EXIST)
				{
					return &_ht[hashi];
				}
				++hashi;//线性探测
				hashi %= _ht.size();
			}
			return nullptr;
		}

		// 删除
		bool Erase(const K& key)
		{
			Node *hashi = Find(key);
			if (hashi)
			{
				hashi->_state ==DELETE;
				--_ht.size();
			}
			else
			{
				return false;
			}

		}

		size_t Size()const
		{
			return _size;
		}

		bool Empty() const
		{
			return _size == 0;
		}

		void Swap(HashTable<K, V>& ht)
		{
			swap(_size, ht._size);
			swap(_totalSize, ht._totalSize);
			_ht.swap(ht._ht);
		}
		
	private:
		size_t HashFunc(const K& key)
		{
			return key % _ht.capacity();
		}
		//void CheckCapacity();
		vector<Node*> _ht;
		size_t _size;
		size_t _totalSize;  // 哈希表中的所有元素：有效和已删除, 扩容时候要用到
	};
}