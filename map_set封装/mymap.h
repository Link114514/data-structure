#pragma once
#include"RBtree_bit.h"
using namespace std;
namespace kp {
	template<class K,class V>
	class map
	{
		struct MapOfKey
		{
			const K&operator()(const pair<K,V> &kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapOfKey>::iterator iterator;
		iterator begin()
		{
			return _BaseTree.begin();
		}
		iterator end()
		{
			return _BaseTree.end();
		}
		bool insert(const pair<K, V>& data)
		{
			return _BaseTree.Insert(data);
		}
	


	private:
		RBTree<K, pair<const K, V>, MapOfKey> _BaseTree;

	};


}