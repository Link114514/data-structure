#include "Heap.h"


int main()
{
	
	int a[] = { 4,2,5,6,7,1,9 };
	Heap H1;
	HeapInit(&H1);
	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	{
		HeapPush(&H1, a[i]);
	}
	int k = 7;
	while (k--)
	{
		printf("%d\n", HeapTop(&H1));
		HeapPop(&H1);
	}
	return 0;
}