#pragma once
#include "Hash2 .h"
template<class T>
class unordered_set
{
	struct KeyOfSet
	{
		T&operator()(const T& data)
		{
			return data;
		}
	};
	struct HashCom
	{
		size_t operator()(const T& data)
		{
			return size_t(data);
		}
	};
public:
	typedef typename Hash_Bucket::HashTable<T, T, KeyOfSet, HashCom>::iterator iterator;
	iterator begin()
	{
		return _ht.begin();
	}
	iterator end()
	{
		return _ht.end();
	}
	bool insert(const T& data)
	{
		return _ht.insert(data);
	}
	bool earse(const T& data)
	{
		return _ht.Erase(data);
	}
	iterator find(const T& data)
	{
		return _ht.Find(data);
	}
private:
	Hash_Bucket::HashTable<T,const T, KeyOfSet, HashCom> _ht;
};

