#include "Heap.h"
void HeapInit(Heap* hp) {
	assert(hp);
	hp->a = NULL;
	hp->capacity = 0;
	hp->size = 0;
}

HeapDataType HeapTop(Heap* hp) {
	assert(hp);
	assert(hp->size > 0);
	return hp->a[0];
}

void Swap(int* a, int* b)
{
	HeapDataType* tmp = *a;
	*a = *b;
	*b = tmp;
}
void AdjustUp(HeapDataType*a,int child ) 
{
	int parent = (child - 1) / 2;
	while (child>0)
	{
		if (a[parent] > a[child]) {
			Swap(&a[parent], &a[child]);
			child = parent;
			parent = (child - 1) / 2;
		}	
		else
		{
			break;
		}
	}
}
void AdjustDown(int* a, int size, int parent) 
{
	//假设左孩子小
	int child = parent * 2 + 1;
	 while (child<size)
	{

		if (child+1<size&&a[child + 1] < a[child])
		{
			++child; //右孩子小就变成小的那个
		}
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}


void HeapPush(Heap* hp,HeapDataType x) {
	assert(hp);
	if (hp->capacity==hp->size)
	{
		int newcapacity = hp->capacity == 0 ? 4: hp->capacity*2;
		HeapDataType* tmp = (HeapDataType*)realloc(hp->a, newcapacity * sizeof(HeapDataType));
		if (tmp == NULL) {
			perror("tmp realloc error");
			return ;
		}
		hp->capacity = newcapacity;
		hp->a = tmp;
	}
	hp->a[hp->size] = x;
	hp->size++;
	AdjustUp(hp->a, hp->size-1);
}
void HeapPop(Heap* hp) {
	assert(hp);
	assert(hp->size > 0);
	Swap(&hp->a[0], &hp->a[hp->size - 1]);
	hp->size--;
	AdjustDown(hp->a, hp->size,0);
}

void HeapDestory(Heap* hp) {
	assert(hp);
	free(hp->a);
	hp->capacity = hp->size = 0;
}
bool HeapEmpty(Heap* hp) {
	assert(hp);
	assert(hp->size);
	return hp->size == 0;
}
size_t HpSize(Heap* hp) {
	assert(hp);
	assert(hp->size > 0);
	return hp->size;
}
