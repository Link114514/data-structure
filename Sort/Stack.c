#include "Stack.h"
void StackInit(ST* ps) {
	assert(ps);
	ps->TOP = 0;//要指向栈顶元素，空的时候给1，插入一个时才是0，数组下标
	ps->capacity = 0;
	ps->a = NULL;
}
// 入栈 
void StackPush(ST* ps, STDataType data) { 
		assert(ps);
		if (ps->TOP == ps->capacity)
		{
			int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
			STDataType* tmp = (STDataType*)realloc(ps->a, sizeof(STDataType) * newcapacity);
			if (tmp == NULL)
			{
				perror("realloc fail");
				return;
			}

			ps->a = tmp;
			ps->capacity = newcapacity;
		}

		ps->a[ps->TOP] = data;
		ps->TOP++;
}
// 出栈 
void StackPop(ST* ps) {
	assert(ps);
	assert(ps->TOP > 0);
	ps->TOP--;
}
// 获取栈顶元素 
STDataType StackTop(ST* ps) {
	assert(ps);
	assert(ps->TOP > 0);
	return ps->a[ps->TOP-1];
}

//打印
void StackPrint(ST* ps) {
	assert(ps);
	for (int i = ps->TOP-1; i >=0 ; i--)
	{
		printf("|%d|\n", ps->a[i]);
	}
}

// 获取栈中有效元素个数 
int StackSize(ST* ps) {
	assert(ps);
	return ps->TOP;
}
// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 

// 销毁栈 
void StackDestroy(ST* ps) {
	assert(ps);
	free(ps->a);
	ps->a = NULL;
	ps->capacity = ps->TOP = 0;    
}
//判断是否为空
bool Stackbool(ST* ps) {
	assert(ps);
	return ps->TOP == 0;
}