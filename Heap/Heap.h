#pragma once
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <errno.h>
#include <assert.h>
#include <stdbool.h>


typedef int HeapDataType;
typedef struct Heap
{
    HeapDataType* a;
    int size;
    int capacity;
}Heap;

void HeapInit(Heap* hp);
void HeapPush(Heap* hp, HeapDataType x);
void HeapPop(Heap* hp);
void HeapDestory(Heap* hp);
void Swap(int* a, int* b);
void AdjustUp(HeapDataType* a, int child);
HeapDataType HeapTop(Heap* hp);
size_t HpSize(Heap* hp);
bool HeapEmpty(Heap* hp);
