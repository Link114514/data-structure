#include "List.h"
//初始化链表
//建议画图 方便理解
Ltnode* LTInit() {
	Ltnode*phead = (Ltnode*)malloc(sizeof(Ltnode));//phead为哨兵位
	if ((phead) == NULL) {
		perror("malloc");
		return ;
	}
	phead->data = -1;//哨兵位随意赋个值
	phead->next =phead->prev= phead;

	return phead;
}
//建立一个节点
Ltnode* BuyNode(LtDataType x) {
	Ltnode* node = (Ltnode*)malloc(sizeof(Ltnode));
	if (node==NULL)
	{
		perror("malloc");
	}
	node->data = x;
	node->next = node->prev=NULL;
	return node;
}
//尾插
void LTPushBack(Ltnode* phead, LtDataType x) {
	assert(phead);
	Ltnode* node = BuyNode(x);
	//处理新节点的前驱和后驱节点
	node->prev = phead->prev;
	node->next = phead;
	//处理新尾节点和phead
	phead->prev->next = node;
	phead->prev = node;
}
//头删
void PopFront(Ltnode* phead) {
	assert(phead);
	assert(phead->next != phead);
	Ltnode* del = phead->next;
	del->next->prev = phead;
	phead->next = del->next;
	free(del);
	del = NULL; 
}
//尾删
void PopBack(Ltnode* phead) {
	assert(phead);
	assert(phead->next != phead);
	Ltnode* del = phead->prev;
	del->prev->next = phead;
	phead->prev = del->prev;
	free(del);
	del = NULL;
}
//打印
void LTPrint(Ltnode* phead) {
	if (phead==NULL)	{
		printf("NULL");
	}
	else
	{
		Ltnode* cur = phead->next;
		if (cur == NULL) {
			printf("NULL");
		}
		while (cur != phead)
		{
			printf("%d->", cur->data);
			cur = cur->next;
		}
	}
	printf("\n");  
}
//
//void LTDestroy(Ltnode* phead) {
//	assert(phead);
//	Ltnode* cur = phead->next;
//	while (cur!=phead){
//	Ltnode* next = cur->next;
//	 free(cur);
//	 cur=next;
//	}
//	free(phead);
//	phead = NULL;
//}
void LTDestroy2(Ltnode** pphead) {
	assert(pphead);
	assert(*pphead);
	if (pphead==NULL||*pphead==NULL)
	{
		return;
	}
	Ltnode* cur = (*pphead)->next;
	while (cur!=*pphead)
	{
		Ltnode* next = cur->next;
		free(cur);
		cur = next;
	}
	free(*pphead);
	*pphead = NULL;
}
void LTInsertAfter(Ltnode* pos, LtDataType x) {
	assert(pos);
	Ltnode* node = BuyNode(x);
	node->next = pos->next;
	node->prev = pos;
	pos->next = node;
	node->next->prev = node;

}
void ListErase(Ltnode* pos) {
	
}
Ltnode* LTFind(Ltnode* phead, LtDataType x) {
	assert(phead); 
	Ltnode* cur = phead->next;
	while (cur!=phead)
	{
		if (cur->data==x) {
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

void LTErase(Ltnode* pos) {
	pos->next->prev = pos->prev;
	pos->prev->next = pos->next;
	free(pos);
	pos = NULL;
}